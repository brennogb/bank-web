package br.com.bankweb.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bankweb.model.Usuario;

public class AutenticacaoFilter implements Filter {

	private static final String CAMINHO_BASE = "/bank-web/";
	private static final Map<String, String> ROTAS_LIVRES = new HashMap<>();

	static {
		ROTAS_LIVRES.put("caminhoBase", CAMINHO_BASE);
		ROTAS_LIVRES.put("login", CAMINHO_BASE + "login.xhtml");
		ROTAS_LIVRES.put("cadastro", CAMINHO_BASE + "cadastro.xhtml");
	}

	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String uri = httpServletRequest.getRequestURI();
		Usuario usuarioLogado = (Usuario) httpServletRequest.getSession().getAttribute("usuarioLogado");

		if (usuarioLogado != null && uri.equals(ROTAS_LIVRES.get("login"))) {
			((HttpServletResponse)response).sendRedirect(CAMINHO_BASE + "index.xhtml");
		}
		
		if (usuarioLogado != null || ROTAS_LIVRES.containsValue(uri)) {
			chain.doFilter(request, response);
		} else {
			((HttpServletResponse)response).sendRedirect(ROTAS_LIVRES.get("login"));
		}
	}

	@Override
	public void destroy() {
	}

}
