package br.com.bankweb.util;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.bankweb.model.Usuario;

@Dependent
public class WebUtil {
	public void adicionarUsuarioNaSessao(Usuario usuario) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.setAttribute("usuarioLogado", usuario);
	}
	
	public Usuario obterUsuarioDaSessao() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Usuario usuario = (Usuario) session.getAttribute("usuarioLogado");
		
		return usuario;
	}
	
	public void adicionarMensagemNoContexto(MessageType messageType, String mensagem, String descricao) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(mensagem, descricao));
	}
	
	public String encriptarSenha(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigest[] = algorithm.digest(senha.getBytes("UTF-8"));
		 
		StringBuilder hexString = new StringBuilder();
		for (byte b : messageDigest) {
		  hexString.append(String.format("%02X", 0xFF & b));
		}
		return hexString.toString();
	}
	
}
