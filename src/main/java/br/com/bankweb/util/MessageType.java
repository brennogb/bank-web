package br.com.bankweb.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;

public enum MessageType {
	INFORMATION(FacesMessage.SEVERITY_INFO),
	WARNING(FacesMessage.SEVERITY_WARN),
	ERROR(FacesMessage.SEVERITY_ERROR),
	FATAL(FacesMessage.SEVERITY_FATAL);
	
	Severity severity;
	
	MessageType(Severity severity) {
		this.severity = severity;
	}
	
	Severity getSeverity() {
		return severity;
	}

}
