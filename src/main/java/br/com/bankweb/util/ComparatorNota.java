package br.com.bankweb.util;

import java.util.Comparator;

import br.com.bankweb.model.EstoqueNota;

public class ComparatorNota implements Comparator<EstoqueNota> {

	@Override
	public int compare(EstoqueNota o1, EstoqueNota o2) {
		return o1.getValorNota().compareTo(o2.getValorNota());
	}

}
