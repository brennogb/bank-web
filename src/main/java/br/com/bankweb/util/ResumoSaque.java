package br.com.bankweb.util;

import java.util.List;

import br.com.bankweb.model.EstoqueNota;

/**
 * Classe que define uma combinação para se obter o valor do saque
 * @author Brenno Gomes
 *
 */
public class ResumoSaque {

	private List<EstoqueNota> notasSaque;

	public List<EstoqueNota> getNotasSaque() {
		return notasSaque;
	}

	public void setNotasSaque(List<EstoqueNota> notasSaque) {
		this.notasSaque = notasSaque;
	}
	
}
