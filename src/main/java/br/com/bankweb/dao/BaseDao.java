package br.com.bankweb.dao;

import java.util.List;

public interface BaseDao<T> {
	
	void inserir(T entidade);
	
	void atualizar(T entidade);
	
	void excluir(short id);
	
	List<T> listar();
	
	T obterPorId(long id);
}
