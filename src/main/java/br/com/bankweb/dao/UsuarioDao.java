package br.com.bankweb.dao;

import br.com.bankweb.model.Usuario;

public interface UsuarioDao extends BaseDao<Usuario> {
	
	Usuario obterUsuarioPorLogin(String login);
}
