package br.com.bankweb.dao.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.bankweb.config.Transacional;
import br.com.bankweb.dao.EstoqueDao;
import br.com.bankweb.model.EstoqueNota;

@Transacional
public class EstoqueDaoImpl implements EstoqueDao {

	@Inject
	private EntityManager manager;
	
	@Override
	public void inserir(EstoqueNota entidade) {
		manager.persist(entidade);
	}

	@Override
	public void atualizar(EstoqueNota entidade) {
		manager.merge(entidade);
	}

	@Override
	public void excluir(short id) {	
		EstoqueNota nota = manager.getReference(EstoqueNota.class, id);
		manager.remove(nota);
	}

	@Override
	public List<EstoqueNota> listar() {
		final Query query = manager.createQuery("select a from EstoqueNota a");
		List<EstoqueNota> result = query.getResultList();
		return result;
	}

	@Override
	public EstoqueNota obterPorId(long id) {
		
		return null;
	}

	@Override
	public EstoqueNota obterEstoqueNota(short valorNota) {
		EstoqueNota estoqueNota = manager.find(EstoqueNota.class, valorNota);
		return estoqueNota;
	}

	@Override
	public List<EstoqueNota> obterNotasDisponiveisSaque(int valorMinimo) {
		final Query query = manager.createQuery("select a from EstoqueNota a where a.quantidade > :valorMinimo");
		query.setParameter("valorMinimo", valorMinimo);
		List<EstoqueNota> result = query.getResultList();
		return result;
	}
}
