package br.com.bankweb.dao.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.bankweb.config.Transacional;
import br.com.bankweb.dao.UsuarioDao;
import br.com.bankweb.model.Usuario;

@Transacional
public class UsuarioDaoImpl implements UsuarioDao {

	@Inject
	private EntityManager manager;
	
	@Override
	public void inserir(Usuario entidade) {
		manager.persist(entidade);
	}

	@Override
	public void atualizar(Usuario entidade) {
		manager.merge(entidade);
	}

	@Override
	public void excluir(short id) {
		Usuario usuario = manager.getReference(Usuario.class, id);
		manager.remove(usuario);
	}

	@Override
	public List<Usuario> listar() {
		final Query query = manager.createQuery("select u from Usuario u");
		List<Usuario> result = query.getResultList();
		return result;
	}

	@Override
	public Usuario obterPorId(long id) {
		return manager.find(Usuario.class, id);
	}

	@Override
	public Usuario obterUsuarioPorLogin(String login) {
		final Query query = manager.createQuery("select u from Usuario u where u.login = :login");
		query.setParameter("login", login);
		Usuario usuario = (Usuario) query.getSingleResult();
		return usuario;
	}

}
