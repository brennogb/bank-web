package br.com.bankweb.dao;

import java.util.List;

import br.com.bankweb.model.EstoqueNota;

public interface EstoqueDao extends BaseDao<EstoqueNota> {
	
	EstoqueNota obterEstoqueNota(short valorNota);
	
	List<EstoqueNota> obterNotasDisponiveisSaque(int valorMinimo);
}
