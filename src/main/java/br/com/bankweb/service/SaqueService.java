package br.com.bankweb.service;

import java.util.List;

import br.com.bankweb.model.EstoqueNota;
import br.com.bankweb.util.ResumoSaque;

public interface SaqueService {
	
	List<ResumoSaque> sacarNotasSelecionadas(short valorSaque, List<String> notasSelecionadas);
	
	List<EstoqueNota> sacarTodasAsNotas(short valorSaque);
	
}
