package br.com.bankweb.service;

import java.util.List;

import br.com.bankweb.model.EstoqueNota;

public interface EstoqueService {

	EstoqueNota obterEstoqueNota(short valorNota);
	
	void salvarNota(EstoqueNota estoqueNota);
	
	List<EstoqueNota> obterNotas();
	
	void excluirNota(EstoqueNota estoqueNota);
	
	List<EstoqueNota> obterNotasDisponiveisSaque();
	
	List<EstoqueNota> obterNotasDisponiveisSaque(List<String> notasSelecionadas);
	
}
