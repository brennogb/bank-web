package br.com.bankweb.service;

import br.com.bankweb.model.Usuario;

public interface UsuarioService {

	
	boolean autenticarUsuario(Usuario usuario);
	
	Usuario obterUsuarioPorLogin(String login);
	
	void cadastrarUsuario(Usuario usuario);
}
