package br.com.bankweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.bankweb.dao.EstoqueDao;
import br.com.bankweb.model.EstoqueNota;
import br.com.bankweb.service.EstoqueService;

public class EstoqueServiceImpl implements EstoqueService {

	@Inject
	private EstoqueDao estoqueDao;
	
	private static final int MINIMO_QUANTIDADE = 5;
	
	@Override
	public EstoqueNota obterEstoqueNota(short valorNota) {
		return estoqueDao.obterEstoqueNota(valorNota);
	}

	@Override
	public void salvarNota(EstoqueNota estoqueNota) {
		EstoqueNota nota = estoqueDao.obterEstoqueNota(estoqueNota.getValorNota());
		
		if (nota != null) {
			estoqueDao.atualizar(estoqueNota);
		} else {
			estoqueDao.inserir(estoqueNota);
		}
	}

	@Override
	public List<EstoqueNota> obterNotas() {
		return estoqueDao.listar();
	}

	@Override
	public void excluirNota(EstoqueNota estoqueNota) {
		estoqueDao.excluir(estoqueNota.getValorNota());
	}

	@Override
	public List<EstoqueNota> obterNotasDisponiveisSaque() {
		List<EstoqueNota> notasDisponiveis = estoqueDao.obterNotasDisponiveisSaque(MINIMO_QUANTIDADE);
		return notasDisponiveis;
	}

	@Override
	public List<EstoqueNota> obterNotasDisponiveisSaque(List<String> notasSelecionadas) {
		List<EstoqueNota> result = new ArrayList<>();
		
		for (int i = 0; i < notasSelecionadas.size(); i++) {
			result.add(estoqueDao.obterEstoqueNota(Short.parseShort(notasSelecionadas.get(i))));
		}
		
		return result;
	}

}
