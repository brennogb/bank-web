package br.com.bankweb.service.impl;

import javax.inject.Inject;

import br.com.bankweb.dao.UsuarioDao;
import br.com.bankweb.model.Usuario;
import br.com.bankweb.service.UsuarioService;

public class UsuarioServiceImpl implements UsuarioService {

	
	@Inject
	private UsuarioDao usuarioDao;
	
	@Override
	public boolean autenticarUsuario(Usuario usuario) {
		Usuario usuarioBanco = usuarioDao.obterUsuarioPorLogin(usuario.getLogin());
		if (usuarioBanco == null) {
			return false;
		}
		
		if (!usuario.getSenha().equals(usuarioBanco.getSenha())) {
			return false;
		}
		
		return true;
	}

	@Override
	public Usuario obterUsuarioPorLogin(String login) {
		return usuarioDao.obterUsuarioPorLogin(login);
	}

	@Override
	public void cadastrarUsuario(Usuario usuario) {
		usuarioDao.inserir(usuario);
	}
	
}
