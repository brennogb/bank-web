package br.com.bankweb.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import br.com.bankweb.model.EstoqueNota;
import br.com.bankweb.service.EstoqueService;
import br.com.bankweb.service.SaqueService;
import br.com.bankweb.util.ComparatorNota;
import br.com.bankweb.util.ResumoSaque;

public class SaqueServiceImpl implements SaqueService {

	@Inject
	private EstoqueService estoqueService;

	@Override
	public List<ResumoSaque> sacarNotasSelecionadas(short valorSaque, List<String> notasSelecionadas) {
		List<ResumoSaque> result = new ArrayList<>();
		List<EstoqueNota> notasDisponiveis = estoqueService.obterNotasDisponiveisSaque(notasSelecionadas);

		// quando � selecionado apenas uma nota, ele verifica se � possivel realizar o
		// saque mesmo assim
		if (notasDisponiveis.size() == 1) {
			int quantidadeNota = realizarSaqueNotaUnica(valorSaque, notasDisponiveis.get(0));

			if (quantidadeNota > 0) {
				ResumoSaque resumo = new ResumoSaque();
				resumo.setNotasSaque(
						Arrays.asList(new EstoqueNota(notasDisponiveis.get(0).getValorNota(), quantidadeNota)));
				result.add(resumo);
			}

			return result;
		}

		// combinacao das notas com at� 2 c�dulas. Adicionar combina��o com 3
		List<ResumoSaque> notasCombinadas = combinarNotas(notasDisponiveis, valorSaque);

		result = popularQuantidadeSaque(notasCombinadas, valorSaque);

		return result;
	}

	private List<ResumoSaque> popularQuantidadeSaque(List<ResumoSaque> notasCombinadas, short valorSaque) {
		List<ResumoSaque> result = new ArrayList<>();

		for (ResumoSaque resumo : notasCombinadas) {
			List<EstoqueNota> combinacao = resumo.getNotasSaque();
			zerarQuantidadeCombinacao(combinacao);
			int valorObtido = 0;
			int valorAux = 0;
			int tentativas = 0;

			while (valorObtido < valorSaque) {
				for (EstoqueNota nota : combinacao) {
					valorAux += nota.getValorNota();

					if (valorAux > valorSaque) {
						continue;
					}

					valorObtido += nota.getValorNota();

					if (valorObtido > valorSaque) {
						valorObtido -= nota.getValorNota();
						continue;
					}

					nota.setQuantidade(nota.getQuantidade() + 1);
				}

				if (valorAux > valorObtido && valorObtido < valorSaque && tentativas <= 5) {
					valorAux = valorObtido;
					tentativas++;
				}

				if (valorObtido > valorSaque || valorObtido == valorSaque || tentativas == 5) {
					break;
				}
			}

			if (tentativas == 5) {
				zerarQuantidadeCombinacao(combinacao);
				valorObtido = realizarSaqueMaiorParaMenorNota(combinacao, valorSaque);
			}

			if (valorObtido == valorSaque && validarUsoTodasNotas(resumo)) {
				result.add(resumo);
			}
		}

		return result;
	}

	/**
	 * Valida se todas as notas da combinacao foram utilizadas
	 * 
	 * @param resumo
	 * @return
	 */
	private boolean validarUsoTodasNotas(ResumoSaque resumo) {

		for (EstoqueNota item : resumo.getNotasSaque()) {
			if (item.getQuantidade() == 0) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Tenta realizar o saque do maior para menor nota, caso a primeira estrategia
	 * n�o funcione
	 * 
	 * @param notas
	 * @return
	 */
	public int realizarSaqueMaiorParaMenorNota(List<EstoqueNota> notas, short valorSaque) {
		int valorSaqueAux = valorSaque;
		int valorRestante = 1;

		Comparator<EstoqueNota> comparatorReverse = Collections.reverseOrder(new ComparatorNota());

		notas.sort(comparatorReverse);

		for (EstoqueNota nota : notas) {
			int qtdUsada = valorSaqueAux / nota.getValorNota();
			nota.setQuantidade(qtdUsada);
			valorRestante = valorSaqueAux % nota.getValorNota();

			if (valorRestante > 0) {
				valorSaqueAux = valorRestante;
			}
		}

		// Nao conseguiu obter o saque da maior para menor
		if (valorRestante > 0) {
			zerarQuantidadeCombinacao(notas);
			return 0;
		}

		return valorSaque;
	}

	@Override
	public List<EstoqueNota> sacarTodasAsNotas(short valorSaque) {
		List<EstoqueNota> result = new ArrayList<>();
		List<String> notas = Arrays.asList("10", "20", "50", "100");
		
		List<ResumoSaque> resumo = sacarNotasSelecionadas(valorSaque, notas);
		
		result = filtrarMelhorOpcaoSaque(resumo);
		
		debitarSaldoNotas(result);
		
		return result;
	}
	
	private void debitarSaldoNotas(List<EstoqueNota> notas) {
		for (EstoqueNota estoqueNota : notas) {
			EstoqueNota estoqueNotaBanco = estoqueService.obterEstoqueNota(estoqueNota.getValorNota());
			int quantidadeFinal = estoqueNotaBanco.getQuantidade() - estoqueNota.getQuantidade();
			
			estoqueService.salvarNota(new EstoqueNota(estoqueNota.getValorNota(), quantidadeFinal));
		}
	}

	private List<EstoqueNota> filtrarMelhorOpcaoSaque(List<ResumoSaque> resumo) {
		int qtdNotasAnterior = 0;
		List<EstoqueNota> result = new ArrayList<>();
		for (ResumoSaque resumoSaque : resumo) {
			int qtdNotasAtual = 0;
			for (EstoqueNota nota : resumoSaque.getNotasSaque()) {
				qtdNotasAtual += nota.getQuantidade();
			}
			
			if (qtdNotasAnterior == 0) {
				result = resumoSaque.getNotasSaque();
			} else {
				if (qtdNotasAtual < qtdNotasAnterior) {
					result = resumoSaque.getNotasSaque();
				}
			}
			
			qtdNotasAnterior = qtdNotasAtual;
		}
		return result;
	}

	private List<ResumoSaque> combinarNotas(List<EstoqueNota> notasDisponiveis, short valorSaque) {
		List<ResumoSaque> result = new ArrayList<>();
		
		if (notasDisponiveis.size() == 1) {
			for (EstoqueNota nota : notasDisponiveis) {
				ResumoSaque resumo = new ResumoSaque();
				resumo.setNotasSaque(Arrays.asList(new EstoqueNota(nota.getValorNota(), 0)));
				result.add(resumo);
			}
		}

		for (int i = 0; i < notasDisponiveis.size() - 1; i++) {
			for (int j = i + 1; j < notasDisponiveis.size(); j++) {
				ResumoSaque resumo = new ResumoSaque();
				resumo.setNotasSaque(Arrays.asList(new EstoqueNota(notasDisponiveis.get(i).getValorNota(), 0),
						new EstoqueNota(notasDisponiveis.get(j).getValorNota(), 0)));

				if (validarCombinacaoNotas(resumo.getNotasSaque(), valorSaque)) {
					result.add(resumo);
				}
			}
		}

		result.addAll(combinarNotasTresDigitos(notasDisponiveis, valorSaque));

		return result;
	}

	private List<ResumoSaque> combinarNotasTresDigitos(List<EstoqueNota> notasDisponiveis, short valorSaque) {
		List<ResumoSaque> result = new ArrayList<>();

		for (int i = 0; i < notasDisponiveis.size(); i++) {
			for (int j = i + 1; j < notasDisponiveis.size() - 1; j++) {
				List<EstoqueNota> combinacao = new ArrayList<>();
				combinacao.add(new EstoqueNota(notasDisponiveis.get(i).getValorNota(), 0));
				combinacao.add(new EstoqueNota(notasDisponiveis.get(j).getValorNota(), 0));
				combinacao.add(new EstoqueNota(notasDisponiveis.get(j+1).getValorNota(), 0));
				if (validarCombinacaoNotas(combinacao, valorSaque)) {
					ResumoSaque resumo = new ResumoSaque();
					resumo.setNotasSaque(combinacao);
					result.add(resumo);
				}
			}
		}

		return result;
	}

	private void zerarQuantidadeCombinacao(List<EstoqueNota> notas) {
		for (EstoqueNota estoqueNota : notas) {
			estoqueNota.setQuantidade(0);
		}
	}

	private boolean validarCombinacaoNotas(List<EstoqueNota> notasSaque, short valorSaque) {
		short valorTotal = 0;
		for (EstoqueNota estoqueNota : notasSaque) {
			valorTotal += estoqueNota.getValorNota();
		}

		if (valorTotal > valorSaque) {
			return false;
		}

		return true;
	}

	private int realizarSaqueNotaUnica(short valorSaque, EstoqueNota nota) {
		int result = 0;
		short valorObtido = 0;
		int quantidadeNota = nota.getQuantidade();
		while (quantidadeNota > 0) {
			valorObtido += nota.getValorNota();
			quantidadeNota--;

			if (valorObtido > valorSaque) {
				valorObtido -= nota.getValorNota();
				quantidadeNota++;
				break;
			}
		}

		if (valorObtido == valorSaque) {
			result = quantidadeNota;
		}

		return nota.getQuantidade() - result;
	}
}
