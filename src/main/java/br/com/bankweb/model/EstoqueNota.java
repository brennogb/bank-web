package br.com.bankweb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "ESTOQUE_NOTAS")
@Entity
public class EstoqueNota implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1501844364432031219L;
	
	public EstoqueNota() {
		
	}
	
	public EstoqueNota(Short valorNota, Integer quantidade) {
		this.valorNota = valorNota;
		this.quantidade = quantidade;
	}

	@Column(name = "VALOR_NOTA")
    @Id
    private Short valorNota;
	
	@Column(name = "QUANTIDADE")
    private Integer quantidade;

	public Short getValorNota() {
		return valorNota;
	}

	public void setValorNota(Short valorNota) {
		this.valorNota = valorNota;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((valorNota == null) ? 0 : valorNota.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstoqueNota other = (EstoqueNota) obj;
		if (valorNota == null) {
			if (other.valorNota != null)
				return false;
		} else if (!valorNota.equals(other.valorNota))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.quantidade + " x " + this.valorNota;
	}
}
