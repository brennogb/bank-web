package br.com.bankweb.config;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;

@Transacional
@Interceptor
public class GerenciadorTransacao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1051843317338218483L;
	
	@Inject
	private EntityManager manager;
	
	@AroundInvoke
	public Object executaTX(InvocationContext contexto) throws Exception {

		manager.getTransaction().begin();
		
		// chamar os daos que precisam de um TX
		Object resultado = contexto.proceed();

		manager.getTransaction().commit();

		return resultado;
	}
}
