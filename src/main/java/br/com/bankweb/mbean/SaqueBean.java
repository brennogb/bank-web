package br.com.bankweb.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.bankweb.model.EstoqueNota;
import br.com.bankweb.service.SaqueService;
import br.com.bankweb.util.MessageType;
import br.com.bankweb.util.ResumoSaque;
import br.com.bankweb.util.WebUtil;


@Named
@ViewScoped
public class SaqueBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8442402502241319560L;

	@Inject
	private transient SaqueService saqueService;
	
	@Inject
	private transient WebUtil webUtil;
	
	private List<String> notasSelecionadas;
	
	private short valorSaque;
	
	private List<ResumoSaque> resumo;
	
	private List<String> combinacoes;
	
	private List<EstoqueNota> combinacaoSaque;
	
	private String combinacaoSelecionada;
	
	@PostConstruct
	public void init() {
		resumo = new ArrayList<>();
		combinacaoSaque = new ArrayList<>();
		notasSelecionadas = new ArrayList<>();
		combinacoes = new ArrayList<>();
	}
	
	public void sacar() {
		
		if (valorSaque <= 0) {
			webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "ERRO", "Valor de saque inv�lido");
			return;
		}
		
		if (notasSelecionadas != null && !notasSelecionadas.isEmpty()) {
			 resumo = saqueService.sacarNotasSelecionadas(valorSaque, notasSelecionadas);
			 if (resumo.isEmpty()) {
				webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "ERRO", "N�o foi poss�vel realizar o saque");
				return;
			}
			 combinacoes = popularCombinacoes(resumo);
		} else {
			combinacaoSaque = saqueService.sacarTodasAsNotas(valorSaque);
			if (combinacaoSaque.isEmpty()) {
				webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "ERRO", "N�o foi poss�vel realizar o saque");
				return;
			}
			combinacoes = new ArrayList<>();
			StringBuilder builder = new StringBuilder();
			for (EstoqueNota nota : combinacaoSaque) {
				builder.append(nota.toString()).append(" + ");
			}
			combinacoes.add(builder.toString().trim().substring(0, builder.length() - 2));
		}
	}
	
	private List<String> popularCombinacoes(List<ResumoSaque> resumo) {
		List<String> retorno = new ArrayList<>();
		
		for (ResumoSaque item : resumo) {
			StringBuilder builder = new StringBuilder();
			for (EstoqueNota nota : item.getNotasSaque()) {
				builder.append(nota.toString()).append(" + ");
			}
			retorno.add(builder.toString().trim().substring(0, builder.length() - 2));
		}
		
		return retorno;
	}

	public List<String> getNotasSelecionadas() {
		return notasSelecionadas;
	}

	public void setNotasSelecionadas(List<String> notasSelecionadas) {
		this.notasSelecionadas = notasSelecionadas;
	}

	public short getValorSaque() {
		return valorSaque;
	}

	public void setValorSaque(short valorSaque) {
		this.valorSaque = valorSaque;
	}

	public List<EstoqueNota> getCombinacaoSaque() {
		return combinacaoSaque;
	}

	public void setCombinacaoSaque(List<EstoqueNota> combinacaoSaque) {
		this.combinacaoSaque = combinacaoSaque;
	}

	public List<ResumoSaque> getResumo() {
		return resumo;
	}

	public void setResumo(List<ResumoSaque> resumo) {
		this.resumo = resumo;
	}

	public List<String> getCombinacoes() {
		return combinacoes;
	}

	public void setCombinacoes(List<String> combinacoes) {
		this.combinacoes = combinacoes;
	}

	public String getCombinacaoSelecionada() {
		return combinacaoSelecionada;
	}

	public void setCombinacaoSelecionada(String combinacaoSelecionada) {
		this.combinacaoSelecionada = combinacaoSelecionada;
	}
}
