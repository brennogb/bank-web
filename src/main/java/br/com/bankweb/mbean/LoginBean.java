package br.com.bankweb.mbean;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.bankweb.model.Usuario;
import br.com.bankweb.service.UsuarioService;
import br.com.bankweb.util.MessageType;
import br.com.bankweb.util.WebUtil;

@RequestScoped
@Named
public class LoginBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8232037286255490688L;
	
	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private WebUtil webUtil;
	
	private Usuario usuario;
	
	@PostConstruct
	public void init() {
		setUsuario(new Usuario());
	}

	public String efetuarLogin() {
		
		if (usuario.getLogin().isEmpty() || usuario.getSenha().isEmpty()) {
			webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "Acesso negado", "Login e/ou senha inválidos!");
			return "";
		}
		
		try {
			usuario.setSenha(webUtil.encriptarSenha(usuario.getSenha()));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "Erro", "Erro ao efetuar login");
			return "";
		}
		
		boolean autenticado = usuarioService.autenticarUsuario(getUsuario());
		
		if (autenticado) {
			webUtil.adicionarUsuarioNaSessao(getUsuario());
			return "index.xhtml?faces-redirect=true";
		} else {
			webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "Acesso negado", "Login e/ou senha inválidos!");
			return "";
		}
		
	}
	
	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login.xhtml?faces-redirect=true";
	}
	
	public String cadastrarUsuario() {
		try {
			usuario.setSenha(webUtil.encriptarSenha(usuario.getSenha()));
			usuarioService.cadastrarUsuario(usuario);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			webUtil.adicionarMensagemNoContexto(MessageType.ERROR, "ERRO", "Erro ao cadastrar usuário!");
			return "";
		}
		
		return "login.xhtml?faces-redirect=true";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
