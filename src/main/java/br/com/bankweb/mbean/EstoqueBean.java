package br.com.bankweb.mbean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.bankweb.model.EstoqueNota;
import br.com.bankweb.service.EstoqueService;

@RequestScoped
@Named
public class EstoqueBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4825112223361037976L;
	
	@Inject
	private EstoqueService estoqueService;
	
	private List<EstoqueNota> estoqueNotas;
	
	private EstoqueNota estoqueNota = new EstoqueNota();
	
	public List<EstoqueNota> getEstoqueNotas() {
		if (estoqueNotas == null) {
			estoqueNotas = estoqueService.obterNotas();
		}
		
		return estoqueNotas;
	}
	
	public String excluirEstoqueNota(EstoqueNota nota) {
		estoqueService.excluirNota(nota);
		estoqueNotas.remove(nota);
		
		return null;
	}
	
	public String alterarEstoqueNota(EstoqueNota nota) {
		estoqueNota = nota;
		return "cadastroNotas?faces-redirect=true";
	}
	
	public String salvarNota() {
		estoqueService.salvarNota(estoqueNota);
		
		return "estoqueNotas?faces-redirect=true";
	}

	public EstoqueNota getEstoqueNota() {
		return estoqueNota;
	}

	public void setEstoqueNota(EstoqueNota estoqueNota) {
		this.estoqueNota = estoqueNota;
	}
}
